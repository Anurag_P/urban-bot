, was on his two-wheeler in Hosakote, when he got a call. He stopped to attend the call, which was a stroke of a luck. He heard a heart-rending sound. It was the cry of puppies. He went to check and found the two puppies who were on the verge of death. Nagabhushan turned out to be their saviour. While one of the puppies was on the verge of drowning in a water drum, the other was stuck between the 

Nagabhushan narrates how the events unfolded. “I had stopped my two-wheeler to attend a phone call. I heard the cries of puppies. I tried to locate them with the help of a few residents. We found one of the puppies in the water drum and the other one between the water drum and the compound wall,’’ said Nagabhushan.

He took out the puppy from the water drum and wiped it with a cotton cloth. “I requested one of the residents to bring a 

. Later I took the puppies to the veterinary hospital for treatment. I restored the puppies with their owner,’’ said Nagabhuahan.

He said that he had advised the owner of the puppies to take measures to ensure their safety. “I told him to have a kennel for the puppies. The owner of the puppies works at a private company. He leaves for the workplace at 10 am and returns home at 7 pm. I told him that he cannot let the puppies outside till he returns home,’’ he said. Many animal lovers and lake activists of Hoskote appreciated Nagabhushan for rescuing two puppies, but he brushes it off. “I happened to be there at the right time to save the two puppies,’’ he said.