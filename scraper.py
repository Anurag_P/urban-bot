from bs4 import BeautifulSoup
from urllib.request import urlopen, Request
from sklearn.metrics.pairwise import cosine_similarity
from embedding_as_service.text.encode import Encoder
from textblob import TextBlob

en = Encoder(embedding='bert', model='bert_base_cased', max_seq_length=20)

base_urls = ['https://bangaloremirror.indiatimes.com/bangalore']
hdr = {'User-Agent': 'Mozilla/5.0'}

title_sim_cut_off = 0.9

# cycling relevance text
with open('ref_article.txt', 'r') as f:
    cycling_text = f.readlines()
    cycling_text = ' '.join(cycling_text)
    ref_vec = en.encode(texts=[cycling_text], pooling='reduce_mean')
print('#' * 100)
print('ref article processed')
print(cycling_text)
# get all urls
sel_site_urls = []
for current_url in base_urls:
    site = current_url
    try:
        req = Request(site, headers=hdr)
        page = urlopen(req)
    except Exception as e:
        print('Request failed')
        print(e)
    soup = BeautifulSoup(page)
    counter = 0
    all_urls = soup.find_all('a')
    for anchor in all_urls:
        print('#' * 100)
        progress = float(counter) * 100 / len(all_urls)
        print(f'progress: {progress:.0f}%')
        print(f'anchor {anchor}')
        counter += 1
        try:
            site_url = anchor.get('href', '/')
            site_url_text = anchor.text
            if 'Lake activist' in site_url_text:
                import ipdb; ipdb.set_trace()
            print(f'site_url {site_url_text}')
            if len(site_url_text) > 25:
                site_vecs = en.encode(texts=[site_url_text], pooling='reduce_mean')
                sim = cosine_similarity(ref_vec[0].reshape(1, -1), site_vecs[0].reshape(1, -1))
                if sim > title_sim_cut_off:
                    print('*' * 50)
                    print(site_url_text)
                    print(sim)
                    sel_site_urls += [site_url]
                else:
                    print('*' * 50)
                    print('not similar : {sim}')
        except Exception as e:
            print('Getting href or anchor text failed')
            print(site_url)
            print(e)
print('#' * 40)
print('site relevance processed')
print('sel_site_urls:')
print(sel_site_urls)

# scrape relevant urls
for site_url in sel_site_urls:
    try:
        req = Request(site_url, headers=hdr)
        page = urlopen(req)
    except Exception as e:
        print('Request failed')
        print(e)
    soup = BeautifulSoup(page)
    try:
        article = soup.find_all('div', {"class": "section1"})
        article = ' '.join([tag.text for tag in article])
        # check if article is relevant
        article_vecs = en.encode(texts=[article], pooling='reduce_mean')
        sim = cosine_similarity(ref_vec[0].reshape(1, -1), article_vecs[0].reshape(1, -1))
        print(sim)
        if sim < .4:
            break
        # sentiment of article
        blob = TextBlob(article)
        for sentence in blob.sentences:
            if sentence.sentiment.polarity > 0:
                # tweet
                print('Yay')
            else:
                # tweet
                print(':(')
    except Exception as e:
        print('scraping or sentiment failed')
        print(e)

print('#' * 40)
print('tweet processed')